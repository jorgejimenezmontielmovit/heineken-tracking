<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_user', function (Blueprint $table) {
            $table->smallIncrements('id_user');
            $table->string('name', 60);
            $table->text('email');
            $table->string('password',20);
            $table->smallInteger('role');
            $table->text('img')->nullable();
            $table->boolean('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_user');
    }
}
