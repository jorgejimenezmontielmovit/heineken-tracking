
window.onload = function(){

	document.getElementById("ejecutar").addEventListener("click", function(){

		let value = document.getElementById("description").value;
		if(value === "") return; 
		
		let data = {
			description: value
		};

		request("post", url + "/create/", data, resp => {
			if(resp.success) alert("El contenedor fue agregado exitosamente")
		});
	})

}

function request(method, url, data, response){
	$.ajax({
		headers:{
			'X-CSRF-Token': csrfToken
		},
		method: method,
		url: url,
		data: data,
		success: response,
		dataType: "json"
	});
}


