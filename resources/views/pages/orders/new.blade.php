@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
    <!-- ------------------------------------------------- -->
    <!-- TITULO -->
    <div class="d-inline-flex mb-3" style="width: 100%;">
        <h4>New</h4>
        <h6 class="ml-auto">Admin > Orders > New</h6>
    </div>
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- FILTROS -->
    <!-- <div class="card">
        <div class="card-body">
            <div class="row  pb-3">
                <div class="col-12">
                    <button class="btn button-ml-filter d-inline-flex" type="button" data-toggle="collapse"
                        data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                        style="align-items: center;">
                        <i class="fa fa-filter" aria-hidden="true" style="margin-right: 5px;"> </i> Add Filters <i
                            class="fa fa-caret-down" aria-hidden="true" style="margin-left: 5px;"> </i>
                    </button>
                </div>
                <div class="ml-3 mr-3">
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Start Date "2000-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Finish Date "2099-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-success">
                        Customers "All"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                </div>
            </div>


            <div id="collapseExample" class="collapse show">
            <hr>
                <form action="">
                    <div class="generalform ml-5 mr-5">

                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-12">
                                <div class="row justify-content-between pt-1">
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Start Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Final Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Customer:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Stage:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-end pt-3">
                            <div class="col col-lg-3">
                                <div class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse"
                                    href="#collapseExample" role="button" aria-expanded="true"
                                    aria-controls="collapseExample">
                                    Apply <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div> -->
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- RESULTADOS -->
    <div class="card generalform">
        <div class="card-body">
            <div class="row justify-content-center pt-1">
                <div class="col col-lg-12">

                    <div class="row justify-content-between pt-1">
                        <div class="col col-12">
                            <h5>Description</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas impedit illo cum velit
                                adipisci maxime, quae fugit eos saepe, sunt dicta reprehenderit a recusandae necessitatibus,
                                aut fuga ducimus mollitia alias.</p>
                        </div>
                    </div>

                    <div class="row justify-content-between pt-1">
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Order" type="text" required />
                                <label for="Order">Order Number</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Heineken_PO" type="text" required disabled />
                                <label for="Heineken_PO">Heineken PO</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Market" type="text" required />
                                <label for="Market">Market</label>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-between pt-1">
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Sold_To" type="text" required />
                                <label for="Sold_To">Sold To</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Ship_To" type="text" required />
                                <label for="Ship_To">Ship To</label>
                                <!-- Select Modificar -->
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Source" type="text" required />
                                <label for="Source">Source</label>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-between pt-1">
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Incoterm" type="text" required />
                                <label for="Incoterm">Incoterm</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Stage" type="text" required />
                                <label for="Stage">Stage</label>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-between pt-1">
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Requested_ETD" type="date"  />
                                <label for="Requested_ETD">Requested ETD</label>
                                <!-- Modificar -->
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Requested_ETA" type="date"  />
                                <label for="Requested_ETA">Requested ETA</label>
                                <!-- Modificar -->
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="position-relative mt-4">
                                <!-- <input id="Proforma" type="checkbox" required />
                                <label for="Proforma">Proforma</label> -->
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col col-lg-12">
                    <hr>
                    <div class="row justify-content-start">
                        <div class="col-6 d-inline-flex">
                            <div class="form-group">
                                <label for="Proforma_file">Proforma Invoice</label>
                                <div class="file-btn Proforma_file">Upload a file</div>
                                <input type="file" class="form-control-file" id="Proforma_file">
                            </div>
                        </div>
                        <div class="col-6 d-inline-flex">
                            <div class="form-group">
                                <label for="Sourcing_file">Sourcing Invoice</label>
                                <div class="file-btn Sourcing_file">Upload a file</div>
                                <input type="file" class="form-control-file" id="Sourcing_file">
                            </div>
                        </div>
                        <div class="col-6 d-inline-flex">
                            <div class="form-group">
                                <label for="Heineken_file">Heineken Invoice</label>
                                <div class="file-btn Heineken_file">Upload a file</div>
                                <input type="file" class="form-control-file" id="Heineken_file">
                            </div>
                        </div>
                        <div class="col-6 d-inline-flex">
                            <div class="form-group">
                                <label for="Shipping_file">Shipping Documents</label>
                                <div class="file-btn Shipping_file">Upload a file</div>
                                <input type="file" class="form-control-file" id="Shipping_file">
                            </div>
                        </div>
                    </div>
                </div>

                

            </div>

        <!-- </div>
    </div>

    <div class="card generalform">
        <div class="card-body"> -->
            <div class="row justify-content-center pt-1 mb-4">
                <div class="col col-lg-12">
                    <hr>
                    <div class="row justify-content-between pt-1">
                        <div class="col col-12">
                            <h5>Products</h5>
                        </div>
                    </div>
                    <div class="row justify-content-between pt-1">
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="ProductDescription" type="text" required />
                                <label for="ProductDescription">Product Description</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Quantity" type="text" required />
                                <label for="Quantity">Quantity in cases</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Containers" type="text" required />
                                <label for="Containers"># of Containers (order line)</label>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between pt-1">
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="Transport" type="text" required />
                                <label for="Transport">Transport Type</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <div class="group-generalform">
                                <input id="FCL" type="text" required />
                                <label for="FCL">(standard FCL in cases)</label>
                            </div>
                        </div>
                        <div class="col col-lg-4">
                            <!-- <div class="group-generalform">
                                <input id="TotalContainers" type="text" required />
                                <label for="TotalContainers">Total Containers (per order)</label>
                            </div> -->
                        </div>
                    </div>
                    <div class="row justify-content-end pt-3">
                        <div class="col col-lg-3">
                            <button class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="mdi mdi-bookmark-plus-outline"></i> Add
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row justify-content-center pt-1">
                <div class="col col-lg-12">

                    <div class="row justify-content-between pt-1">
                        <div class="col col-12">
                            <h5><strong>Total Containers: </strong> <span>Example</span></h5>
                        </div>
                        <div class="col col-12">
                            <div class="table_sticky">
                                <table class="table">
                                    <thead class="widthauto">
                                        <tr>
                                        <th style="width: 100px;">SKU</th><th>Product Description</th><th>ETD</th><th>Quantity</th><th># of Containers</th><th>Transport Type</th><th>Acciones</th></tr></thead><tbody>
                                                <tr><td>112801</td><td>Heineken K2</td><td>10/10/2019</td><td>500</td><td>1</td><td>20</td><td class="d-inline-flex"><i class="mdi mdi-delete"></i>  |  <i class="mdi mdi-lead-pencil"></i></td></tr>
                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
                                                </tbody></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-lg-12 mb-4">
            <hr>
            <div class="row justify-content-end pt-3">
                <div class="col col-lg-3">
                    <button class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse"
                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="mdi mdi-content-save"></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
@endsection