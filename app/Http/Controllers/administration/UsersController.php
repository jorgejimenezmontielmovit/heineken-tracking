<?php

namespace App\Http\Controllers\administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\administration\Users;

use Illuminate\Support\Facades\Crypt;

class UsersController extends Controller
{
    public function index()
    {
        $users = Users::all();
        return view("pages.administration.user", ['users' => $users]);
    }
    public function save(Request $request)
    {
        try
		{
            $mode = new Users;
            $password = Crypt::encryptString($request->password);
            $mode->name = $request->name;
            $mode->email = $request->email;
            $mode->password = $password;
            $mode->role = $request->type;
            $mode->status = $request->status;
			$mode->save();
			return response()->json([ 'ok' => 'success', 'message' => 'Registro creado correctamente' ]);
		}
		catch(\Exception $e)
		{
			$e->getMessage();
			return response()->json([ 'ok' => 'error', 'message' => 'Error al guardar en la base de datos' ]);
		}
    }
    public function update(Request $request)
    {
        try
		{
            $data = Users::where('id_user', $request->id)->update(['name' => $request->name, 'email' => $request->email, 'role' => $request->type, 'status' => $request->status]);
			return response()->json(['ok' => 'success','message' => 'Registro actualizado correctamente']);
		}
		catch(\Exception $e)
		{
			$e->getMessage();
			return response()->json([ 'ok' => 'error', 'message' => 'Error al guardar en la base de datos' ]);
		}
    }
    public function show(Request $request)
    {
        $detail = Users::where('id_user', $request->id)->limit(1)->get();
        return response()->json(['ok' => true,'detail' => $detail]);
    }
}
