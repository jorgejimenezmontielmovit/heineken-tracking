@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
    <!-- ------------------------------------------------- -->
    <!-- TITULO -->
    <div class="d-inline-flex mb-3" style="width: 100%;">
        <h4>Users
        </h4>
        <h6 class="ml-auto">Admin > Administration > Users</h6>
    </div>
    <!-- ------------------------------------------------- -->

    <div class="row justify-content-between pt-3">
        <div class="col col-3 d-inline-flex  generalform">
            <h3 class="card-title-2">Search:</h3> <input type="text">
        </div>
        <div class="col col-3 float-right align-end">
            <div class="d-inline-flex">
                <button class="button-ml" id="otro">
                    <i class="fa fa-file-excel-o"> Excel</i>
                </button>
                <button class="button-ml" id="otro" data-toggle="modal" data-target=".modal_user_new">
                    <i  class="fa fa-plus"> New</i>
                </button>
            </div>
        </div>
    </div>
    <div class="card card-table generalform">
        <div class="card-body">

            <div class="row justify-content-between pt-1">
                <div class="col col-12">

                    <div class="table_sticky">
                        <table class="table">
                            <thead class="">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        @if ($user->role == 0)
                                            <td><label>Administrador</label></td>
                                        @elseif ($user->role == 1)
                                            <td><label>Planta</label></td>
                                        @else
                                            <td><label>Cliente</label></td>
                                        @endif
                                        <td><i class="fa fa-pencil" aria-hidden="true" data-toggle="modal"
                                                data-target=".bd-example-modal-xl" onClick="edit( {{ $user->id_user }} )"></i></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <form class="justify-content-center" id="user" enctype="multipart/form-data" method="post" action="" onsubmit="event.preventDefault(); send('edit_user');">
	@csrf
        <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl p-4" style="max-width: 60vw !important;" role="document">
                <div class="modal-content p-3">
                    <div class="row justify-content-center pt-1 mb-4">
                        <div class="col col-lg-12">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <input id="id_user" type="hidden"/>
                            <div class="row justify-content-between pt-1">
                                <div class="col col-lg-6">
                                    <div class="group-generalform">
                                        <input id="Name_user_edit" type="text" required />
                                        <label for="Name_user_edit">Name</label>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="group-generalform">
                                        <input id="Email_user_edit" type="text" required />
                                        <label for="Email_user_edit">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-between pt-1">
                                <div class="col col-lg-6">
                                    <div class="group-generalform generalform">
                                        <!-- <input id="Minimun_product_new" type="text" required /> -->
                                        <p style="font-size: 13px; color: #999; margin-left:10px;">Type</p>
                                        <select name="Type_user_edit" id="Type_user_edit" style="width:100% !important;">
                                            <option value="0">Administrador</option>
                                            <option value="1">Planta</option>
                                            <option value="2">Cliente</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="group-generalform generalform">
                                        <!-- <input id="Minimun_product_new" type="text" required /> -->
                                        <p style="font-size: 13px; color: #999; margin-left:10px;">Status</p>
                                        <select name="Status_user_edite" id="Status_user_edite" style="width:100% !important;">
                                            <option value="0">Inactivo</option>
                                            <option value="1">Activo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end pt-3">
                                <div class="col col-lg-3">
                                    <button type="submit" id="ejecutar" class="btn btn-danger float-right" aria-expanded="true"
                                        aria-controls="collapseOne">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Modal -->
    <form class="justify-content-center" id="user" enctype="multipart/form-data" method="post" action="" onsubmit="event.preventDefault(); send('new_user');">
	@csrf
        <div class="modal fade modal_user_new" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl p-4" style="max-width: 60vw !important;" role="document">
                <div class="modal-content p-3">

                    <div class="row justify-content-center pt-1 mb-4">
                        <div class="col col-lg-12">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="row justify-content-between pt-1">
                                <div class="col col-lg-6">
                                    <div class="group-generalform">
                                        <input id="Name_user" type="text" required />
                                        <label for="Name_user">Name</label>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="group-generalform">
                                        <input id="Email_user" type="text" required />
                                        <label for="Email_user">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-between pt-1">
                                <div class="col col-lg-6">
                                    <div class="group-generalform generalform">
                                        <!-- <input id="Minimun_product_new" type="text" required /> -->
                                        <p style="font-size: 13px; color: #999; margin-left:10px;">Type</p>
                                        <select name="Type_user" id="Type_user" style="width:100% !important;">
                                            <option value="0">Administrador</option>
                                            <option value="1">Planta</option>
                                            <option value="2">Cliente</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="group-generalform generalform">
                                        <!-- <input id="Minimun_product_new" type="text" required /> -->
                                        <p style="font-size: 13px; color: #999; margin-left:10px;">Status</p>
                                        <select name="Status_user" id="Status_user" style="width:100% !important;">
                                            <option value="0">Inactivo</option>
                                            <option value="1">Activo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-between pt-1">
                                <div class="col col-lg-6">
                                    <div class="group-generalform">
                                        <input id="password" type="password" required />
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end pt-3">
                                <div class="col col-lg-3">
                                    <!--<button type="submit" data-dismiss="modal" class="btn btn-danger float-right" id="ejecutar"
                                        data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                        aria-controls="collapseOne">
                                        <i class="mdi mdi-content-save"></i> Save
                                    </button>-->
                                    <button type="submit" id="ejecutar" class="btn btn-danger float-right" aria-expanded="true"
                                        aria-controls="collapseOne">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
@section('scripts')
<script type="text/javascript">
function send( form )
{
	if( form == 'new_user' )
	{
		var data =
		{
			name:  $("#Name_user").val(),
            email:  $("#Email_user").val(),
            password:  $("#password").val(),
            type:  $("#Type_user").val(),
            status:  $("#Status_user").val()
		}
		var url = "{{ url('admin/users/new') }}";
    }
    if( form == 'edit_user' )
	{
		var data =
		{
            id: $("#id_user").val(),
			name:  $("#Name_user_edit").val(),
            email:  $("#Email_user_edit").val(),
            type:  $("#Type_user_edit").val(),
            status:  $("#Status_user_edite").val()
		}
		var url = "{{ url('admin/users/update') }}";
    }
	$.ajax({
		type: "POST",
		url: url,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		data: data,
		success: function( data )
		{
            var message = ( !data.message ) ? 'Creado Correctamente' : data.message;
			var ok 		= ( !data.ok ) ? 'success' : data.ok;
			alert(message);
			$('#user').trigger("reset");
            location.reload();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { alert('Al parece algo salio mal, intenta más tarde y/o notifica a sistemas'); }
	});
}
function edit ( id )
{
    var url = "{{ url('admin/users/show') }}";
    var data =
    {
        id: id
    }
    $.ajax({
		type: "POST",
		url: url,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		data: data,
		success: function( data )
		{
            document.getElementById('id_user').value = data.detail[0].id_user;
            document.getElementById('Name_user_edit').value = data.detail[0].name;
            document.getElementById('Email_user_edit').value = data.detail[0].email;
            document.getElementById('Type_user_edit').value = data.detail[0].role;
            document.getElementById('Status_user_edite').value = data.detail[0].status;
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { showNotification('Warning', 'Al parece algo salio mal, intenta más tarde y/o notifica a sistemas', 'error'); }
	});
}
</script>
@endsection
@endsection