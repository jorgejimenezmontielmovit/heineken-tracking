@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
    <!-- ------------------------------------------------- -->
    <!-- TITULO -->
    <div class="d-inline-flex mb-3" style="width: 100%;">
        <h4>Customers
        </h4>
        <h6 class="ml-auto">Admin > Catalogs > Customers</h6>
    </div>
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- FILTROS -->
    <!-- <div class="card">
        <div class="card-body">
            <div class="row  pb-3">
                <div class="col-12">
                    <button class="btn button-ml-filter d-inline-flex" type="button" data-toggle="collapse"
                        data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                        style="align-items: center;">
                        <i class="fa fa-filter" aria-hidden="true" style="margin-right: 5px;"> </i> Add Filters <i
                            class="fa fa-caret-down" aria-hidden="true" style="margin-left: 5px;"> </i>
                    </button>
                </div>
                <div class="ml-3 mr-3">
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Start Date "2000-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Finish Date "2099-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-success">
                        Customers "All"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                </div>
            </div>


            <div id="collapseExample" class="collapse show">
            <hr>
                <form action="">
                    <div class="generalform ml-5 mr-5">

                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-12">
                                <div class="row justify-content-between pt-1">
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Start Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Final Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Customer:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Stage:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-end pt-3">
                            <div class="col col-lg-3">
                                <div class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse"
                                    href="#collapseExample" role="button" aria-expanded="true"
                                    aria-controls="collapseExample">
                                    Apply <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div> -->
    <!-- ------------------------------------------------- -->
    <!-- ------------------------------------------------- -->
    <!-- RESULTADOS -->
    <div class="row justify-content-between pt-3">
        <div class="col col-3 d-inline-flex  generalform">
            <h3 class="card-title-2">Search:</h3> <input type="text">
        </div>
        <div class="col col-3 float-right align-end">
            <div class="d-inline-flex">
                <button class="button-ml d-inline-flex" style="padding:10px 15px;" id="otro">
                    <i class="fa fa-file-excel-o" aria-hidden="true"> </i><h6 class="ml-2 mb-0"> Excel</h6>
                </button>
                <button class="button-ml" id="otro" data-toggle="modal" data-target=".bd-example-modal-xl">
                    <i  class="fa fa-plus"> New</i>
                </button>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">

            <div class="col col-lg-12 ">
                <div class="table_sticky">
                <table class="table">
            <thead class="">
              <tr>
                <th>Sold To</th><th>Name</th><th>Email</th><th>Country</th><th>Assign to</th><th>Shipping-Address</th><th>Actions</th></tr></thead><tbody>
                  <tr><td>22643</td><td>Example 1</td><td>example.1@correo.com</td><td>México</td><td>Example 1</td><td>Example 1</td><td><i class="fa fa-pencil" aria-hidden="true"></i></td></tr>
                  <tr><td>16476</td><td>Example 2</td><td>example.2@correo.com</td><td>México</td><td>Example 2</td><td>Example 2</td><td><i class="fa fa-pencil" aria-hidden="true"></i></td></tr>
                  <tr><td>32158</td><td>Example 3</td><td>example.3@correo.com</td><td>México</td><td>Example 3</td><td>Example 3</td><td><i class="fa fa-pencil" aria-hidden="true"></i></td></tr>
                  <tr><td>32155</td><td>Example 4</td><td>example.4@correo.com</td><td>México</td><td>Example 4</td><td>Example 4</td><td><i class="fa fa-pencil" aria-hidden="true"></i></td></tr>
                  <tr><td>13586</td><td>Example 5</td><td>example.5@correo.com</td><td>México</td><td>Example 5</td><td>Example 5</td><td><i class="fa fa-pencil" aria-hidden="true"></i></td></tr>
                  <tr><td>35466</td><td>Example 6</td><td>example.6@correo.com</td><td>México</td><td>Example 6</td><td>Example 6</td><td><i class="fa fa-pencil" aria-hidden="true"></i></td></tr>
                 </tbody></table>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl p-4" style="max-width: 60vw !important;" role="document">
            <div class="modal-content p-3">

                <div class="row justify-content-center pt-1 mb-4">
                    <div class="col col-lg-12">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Customers</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-6">
                                <div class="group-generalform">
                                    <input id="Name_user_edit" type="text" required />
                                    <label for="Name_user_edit">Name</label>
                                </div>
                            </div>
                            <div class="col col-lg-6">
                                <div class="group-generalform">
                                    <input id="Email_user_edit" type="text" required />
                                    <label for="Email_user_edit">Email</label>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-6">
                                <div class="group-generalform">
                                    <input id="Type_user_edit" type="text" required />
                                    <label for="Type_user_edit">Country</label>
                                </div>
                            </div>
                            <div class="col col-lg-6">
                                <div class="group-generalform">
                                    <input id="Status_user_edite" type="text" required />
                                    <label for="Status_user_edite">Sold To</label>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-6">
                                <div class="group-generalform">
                                    <input id="Type_user_edit" type="text" required />
                                    <label for="Type_user_edit">Assign to</label>
                                </div>
                            </div>
                            <div class="col col-lg-6">
                                <div class="group-generalform">
                                    <input id="Status_user_edite" type="text" required />
                                    <label for="Status_user_edite">Shipping-Address</label>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end pt-3">
                            <div class="col col-lg-3">
                                <button data-dismiss="modal" class="btn btn-danger float-right" id="ejecutar"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                    <i class="mdi mdi-content-save"></i> Save
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
@endsection