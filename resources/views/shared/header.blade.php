<!-- Ideas -->
<!-- https://modus.medium.com -->
<!-- Local Store -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/view_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/newstyle.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <title>Document</title>
</head>

<body>

    <!-- ---------------------------------------------------------------------------------------------------------------------- -->
    <!-- BarNav -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <!-- ---------------------------------------------------------------------------------------------------------- -->
        <!-- SideBar Button -->

        <a class="nav-link nav-btn" href="#"><i class="fa fa-bars" aria-hidden="true"></i> <span
                class="sr-only"></span></a>

        <!-- ---------------------------------------------------------------------------------------------------------- -->
        <!-- ------------------------------------------------------------------------------------------------------------------ -->
        <!-- Logo img -->
        <a class="navbar-brand" href="#"><img src="{{ asset('img/logo.png') }}" alt=""></a>
        <!-- ------------------------------------------------------------------------------------------------------------------ -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="mr-auto"></div>
            <ul class="navbar-nav ">
                <li class="nav-item">
                    <a class="nav-link" href="#">View Ver. 2.5 </a>
                </li>
                <li class="nav-item nav-user dropdown">
                    <img src="{{ asset('img/user.jpg') }}" alt="">
                </li>
            </ul>
        </div>
    </nav>
    <div class="card menu-card hide" style="width: 18rem;">
        <div class="card-body user-card">
            <div class="d-inline-flex up-card">
                <img src="{{ asset('img/user.jpg') }}" alt="">
                <div class="name-card">
                    <h6>UserName</h6>
                    <h6>hola@movit-sg.com</h6>
                </div>
            </div>
            <div class="link-card">
                <ul>
                    <li>
                        <a href="#">
                            <h6><i class="fa fa-cog" aria-hidden="true"></i> Setting</h6>
                        </a>
                    </li>
                    <hr>
                    <li>
                        <a href="#">
                            <h6><i class="fa fa-power-off" aria-hidden="true"></i> Logout</h6>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ---------------------------------------------------------------------------------------------------------------------- -->

    <div class="main-wrapper">