<?php

namespace App\Models\catalogs;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
		protected $table = "cat_product";
		protected $primaryKey = 'id_product';
		public $timestamps = false;
}
