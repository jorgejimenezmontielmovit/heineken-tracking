@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
    <!-- ------------------------------------------------- -->
    <!-- TITULO -->
    <div class="d-inline-flex mb-3" style="width: 100%;">
        <h4>Tracking</h4>
        <h6 class="ml-auto">Admin > Orders > Tracking</h6>
    </div>
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- FILTROS -->
    <!-- <div class="card">
        <div class="card-body">
            <div class="row  pb-3">
                <div class="col-12">
                    <button class="btn button-ml-filter d-inline-flex" type="button" data-toggle="collapse"
                        data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                        style="align-items: center;">
                        <i class="fa fa-filter" aria-hidden="true" style="margin-right: 5px;"> </i> Add Filters <i
                            class="fa fa-caret-down" aria-hidden="true" style="margin-left: 5px;"> </i>
                    </button>
                </div>
                <div class="ml-3 mr-3">
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Start Date "2000-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Finish Date "2099-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-success">
                        Customers "All"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                </div>
            </div>


            <div id="collapseExample" class="collapse show">
            <hr>
                <form action="">
                    <div class="generalform ml-5 mr-5">

                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-12">
                                <div class="row justify-content-between pt-1">
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Start Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Final Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Customer:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Stage:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-end pt-3">
                            <div class="col col-lg-3">
                                <div class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse"
                                    href="#collapseExample" role="button" aria-expanded="true"
                                    aria-controls="collapseExample">
                                    Apply <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div> -->
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- RESULTADOS -->
    <div class="card card-table generalform">
        <div class="card-body">
            <div class="row justify-content-center" style="height: 40px;">
                <div class="col-11">
                    <ul class="nav justify-content-between">
                        <li class="nav-item ">
                            <a class="nav-link" href="#">Received</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Hold</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Credit review</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Confirmed</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Shipping</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Invocing</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Delivered</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Service Rank</a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="row  justify-content-center align-content-center mt-2">
                <div class="col-11">
                    <hr>
                </div>
                <div class="col-5" style="color: gray;">
                    <h6>Actual Status: <strong style="color: #009feb;">Shipping</strong></h6>
                    <h6>New Status: <strong style="color: olivedrab;">Invocing</strong></h6>
                    <hr>
                    <h6>Responsable: <span style="color: black;">Name</span></h6>
                    <h6>Last Modified Date: <span style="color: black;">20/06/19</span></h6>

                </div>
                <div class="col-6">
                    <div class="form-group mb-0">
                        <label for="exampleFormControlTextarea1">Actions</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="row  justify-content-center align-content-center mt-1">
                <div class="col-11">

                    <div class="row justify-content-end pt-3">
                        <div class="col col-lg-3">
                            <button data-dismiss="modal" class="btn btn-danger float-right" id="ejecutar"
                                data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                aria-controls="collapseOne">
                                <i class="mdi mdi-content-save"></i> Save
                            </button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
@endsection