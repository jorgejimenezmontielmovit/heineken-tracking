<?php

namespace App\Http\Controllers\catalogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationsController extends Controller
{
    public function index()
    {
        return view("pages.catalogs.locations");
    }
}
