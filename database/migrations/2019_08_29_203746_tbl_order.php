<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_orders', function (Blueprint $table) {
            $table->smallIncrements('id_order')->index();
            $table->smallInteger('id_customer')->unsigned();
            $table->smallInteger('id_source')->unsigned();
            $table->smallInteger('id_user')->unsigned();
            $table->smallInteger('id_ship')->unsigned();
            $table->string('no_order', 10);
            $table->string('heineken_po', 10);
            $table->timestamp('created_at');
            $table->date('eta');
            $table->date('etd');
            $table->text('file1')->nullable();
            $table->text('file2')->nullable();
            $table->text('file3')->nullable();
            $table->text('file4')->nullable();
            $table->foreign('id_customer')->references('id_customer')->on('cat_customer');
            $table->foreign('id_source')->references('id_source')->on('cat_source');
            $table->foreign('id_user')->references('id_user')->on('cat_user');
            $table->foreign('id_ship')->references('id_ship_to')->on('cat_ship_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_orders');
    }
}
