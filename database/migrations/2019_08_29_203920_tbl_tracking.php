<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tracking', function (Blueprint $table) {
            $table->smallInteger('id_order')->unsigned();
            $table->smallInteger('id_user')->unsigned();
            $table->timestamp('created_at');
            $table->text('comments');
            $table->smallInteger('status')->default(1);
            $table->foreign('id_order')->references('id_order')->on('tbl_orders');
            $table->foreign('id_user')->references('id_user')->on('cat_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tracking');
    }
}
