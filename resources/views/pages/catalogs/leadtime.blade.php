@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
    <!-- ------------------------------------------------- -->
    <!-- TITULO -->
    <div class="d-inline-flex mb-3" style="width: 100%;">
        <h4>Lead Time
        </h4>
        <h6 class="ml-auto">Admin > Catalogs > Lead Time</h6>
    </div>
    <!-- ------------------------------------------------- -->

    <div class="card">
        <div class="card-body">

            <form action="">
                <div class="generalform ml-5 mr-5">

                    <div class="row">
                        <div class="col col-lg-3">
                            <label for="">
                                <h5>Source:</h5>
                                <select id="cobro" name="">
                                    <option value="0">Select One</option>
                                </select>
                            </label>
                        </div>
                        <div class="col col-lg-3">
                            <label for="">
                                <h5>Customer:</h5>
                                <select id="cobro" name="">
                                    <option value="0">Select One</option>
                                </select>
                            </label>
                        </div>
                        <div class="col col-lg-3">
                            <label for="">
                                <h5>Time:</h5>
                                <input id="" name="" type="number">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col col-lg-12 mb-4">
                <hr>
                    <div class="row justify-content-end pt-3">
                        <div class="col col-lg-3">
                            <button type="submit" class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="mdi mdi-bookmark-plus-outline"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
<!-- ----------------------------------------------------------------------------------------------- -->
@endsection