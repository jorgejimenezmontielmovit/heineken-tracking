<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_orders_details', function (Blueprint $table) {
            $table->smallInteger('id_order')->unsigned();
            $table->smallInteger('id_product')->unsigned();
            $table->smallInteger('quantity');
            $table->foreign('id_order')->references('id_order')->on('tbl_orders');
            $table->foreign('id_product')->references('id_product')->on('cat_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_orders_details');
    }
}
