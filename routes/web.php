<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.dashboard.index');
})->name('dashboard');


Route::get('token', function(){
	echo csrf_token();
});

// catalogs/shipment type
Route::group(['prefix' => 'catalogs/shipmenttype'], function(){
	Route::get('/', 'catalogs\ShipmenttypeController@index');
	Route::post('/create', 'catalogs\ShipmenttypeController@create');
	Route::get('/read/{id?}', 'catalogs\ShipmenttypeController@read');
	Route::post('/update/{id}', 'catalogs\ShipmenttypeController@update');
	Route::post('/delete/{id}', 'catalogs\ShipmenttypeController@delete');
});

// catalogs/containertype
Route::group(['prefix' => 'catalogs/containertype'], function(){
	Route::get('/', 'catalogs\ContainertypeController@index');
	Route::post('/create', 'catalogs\ContainertypeController@create');
	Route::get('/read/{id?}', 'catalogs\ContainertypeController@read');
	Route::post('/update/{id}', 'catalogs\ContainertypeController@update');
	Route::post('/delete/{id}', 'catalogs\ContainertypeController@delete');
});

// catalogs/sources
Route::group(['prefix' => 'catalogs/sources'], function(){
	Route::get('/', 'catalogs\SourcesController@index');
	Route::post('/create', 'catalogs\SourcesController@create');
	Route::get('/read/{id?}', 'catalogs\SourcesController@read');
	Route::post('/update/{id}', 'catalogs\SourcesController@update');
	Route::post('/delete/{id}', 'catalogs\SourcesController@delete');
});

// catalogs/products
Route::group(['prefix' => 'catalogs/products'], function(){
	Route::get('/', 'catalogs\ProductsController@index')->name('products');
	Route::post('/create', 'catalogs\ProductsController@create');
	Route::get('/read/{id?}', 'catalogs\ProductsController@read');
	Route::post('/update/{id}', 'catalogs\ProductsController@update');
	Route::post('/delete/{id}', 'catalogs\ProductsController@delete');
});

// catalogs/customers
Route::group(['prefix' => 'catalogs/customers'], function(){
	Route::get('/', 'catalogs\CustomersController@index')->name('customers');
	Route::post('/create', 'catalogs\CustomersController@create');
	Route::get('/read/{id?}', 'catalogs\CustomersController@read');
	Route::post('/update/{id}', 'catalogs\CustomersController@update');
	Route::post('/delete/{id}', 'catalogs\CustomersController@delete');
});

Route::group(['prefix' => 'catalogs/container'], function(){
	Route::get('/', 'catalogs\ContainertypeController@index')->name('container');
});

Route::group(['prefix' => 'catalogs/sources'], function(){
	Route::get('/', 'catalogs\SourcesController@index')->name('sources');
});

Route::group(['prefix' => 'catalogs/shipment'], function(){
	Route::get('/', 'catalogs\ShipmenttypeController@index')->name('shipment');
});

Route::group(['prefix' => 'catalogs/portfolio'], function(){
	Route::get('/', 'catalogs\PortfolioController@index')->name('portfolio');
});

Route::group(['prefix' => 'catalogs/locations'], function(){
	Route::get('/', 'catalogs\LocationsController@index')->name('locations');
});

Route::group(['prefix' => 'catalogs/leadtime'], function(){
	Route::get('/', 'catalogs\LeadtimeController@index')->name('leadtime');
});

Route::group(['prefix' => 'admin/users'], function(){
	Route::get('/', 'administration\UsersController@index')->name('users');
	Route::post('/new', 'administration\UsersController@save');
	Route::post('/update', 'administration\UsersController@update');
	Route::post('/show', 'administration\UsersController@show');
});

// Route::fallback(function () {});
