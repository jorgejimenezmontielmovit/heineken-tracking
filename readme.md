# HEINEKEN - Traking



## Descripción

Sistema de traking.



## Tech briefing

* Laravel 5.8
* PHP 7
* Maria db 8

## Instrucciones

1. Clona el proyecto.

Con SSH.
```
git clone git@gitlab.com:ZeroxZ/heineken-traking.git --recursive
```
Con HTTPS.
```
git clone https://gitlab.com/ZeroxZ/heineken-traking.git --recursive
```

2. Navega a la carpeta.

```
cd heineken
```
3. Copia el contenido de .env.example a .env

4. ejecuta:

```
composer install

php artisan key:generate
```
```
php artisan serve
```

## Local Development Server

`http://localhost:8000`

