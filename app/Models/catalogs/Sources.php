<?php

namespace App\Models\catalogs;

use Illuminate\Database\Eloquent\Model;

class Sources extends Model
{
		protected $table = "cat_source";
		protected $primaryKey = 'id_source';
		public $timestamps = false;
}