<?php

namespace App\Http\Controllers\catalogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioController extends Controller
{
    public function index()
    {
        return view("pages.catalogs.portfolio");
    }
}
