<?php

namespace App\Models\catalogs;

use Illuminate\Database\Eloquent\Model;

class Containertype extends Model
{
		protected $table = "cat_container_type";
		protected $primaryKey = 'id_container';
		public $timestamps = false;
}
