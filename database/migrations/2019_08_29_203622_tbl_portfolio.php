<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPortfolio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_portfolio', function (Blueprint $table) {
            $table->smallInteger('id_customer')->unsigned();
            $table->smallInteger('id_product')->unsigned();
            $table->smallInteger('id_ship')->unsigned();
            $table->smallInteger('id_source')->unsigned();
            $table->foreign('id_customer')->references('id_customer')->on('cat_customer');
            $table->foreign('id_product')->references('id_product')->on('cat_product');
            $table->foreign('id_ship')->references('id_ship_to')->on('cat_ship_to');
            $table->foreign('id_source')->references('id_source')->on('cat_source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_portfolio');
    }
}
