<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CatCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_customer', function (Blueprint $table) {
            $table->smallIncrements('id_customer')->index();
            $table->smallInteger('id_sold_to')->unsigned();
            $table->smallInteger('id_contry')->unsigned();
            $table->text('address');
            $table->text('contact_name');
            $table->foreign('id_sold_to')->references('id_sold_to')->on('cat_sold_to');
            $table->foreign('id_contry')->references('id_contry')->on('cat_contry');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_customer');
    }
}
