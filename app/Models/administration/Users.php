<?php

namespace App\Models\administration;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table    = "cat_user";
    protected $fillable = ['id_user','name','email','password','role','status'];
    public $timestamps = false;
}
