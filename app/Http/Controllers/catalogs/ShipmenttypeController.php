<?php

namespace App\Http\Controllers\catalogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\catalogs\Shipmenttype;
use stdClass;

class ShipmenttypeController extends Controller
{

		public function index()
		{
				return view("pages.catalogs.shipmenttype");
		}

		// Some ways to read the input are: $request->all(), $request->input('field1')
		public function create(Request $request)
		{
			$obj = new stdClass;
			$obj->success = false;
			$model = new Shipmenttype();
			$fields = ['description'];

			if($request->has($fields))
			{

				foreach($fields as $field)
				{
					$model->$field = $request->input($field);
				}

				$result = $model->save();
				if($result)
				{
					$obj->success = true;
				}

			}

			return json_encode($obj);
		}

		public function read(Request $request, $id = null)
		{
			$input = $request->input();
			$obj = new stdClass;
			$obj->success = false;
			$obj->input = $input;

			if(!empty($id))
			{
				$model = Shipmenttype::find($id);
				if($model)
				{
					$obj->data = $model;
					$obj->success = true;
				}
			}
			else
			{
				$model = Shipmenttype::all();
				if(count($model))
				{
					$obj->data = $model;
					$obj->success = true;
				}
			}

			return json_encode($obj);

		}

		public function update(Request $request, $id)
		{
			
			$input = $request->input();
			$model = Shipmenttype::find($id);
			$model->save();
		}

		public function delete($id)
		{
			$obj = new stdClass;
			$obj->success = false;
			$model = Shipmenttype::find($id);
			if($model)
			{
				$result = $model->delete();
				if($result)
				{
					$obj->success = true;
				}
			}
			return json_encode($obj);
		}
}
