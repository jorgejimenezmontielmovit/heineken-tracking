@include('shared.header')
@include('shared.sidebar')
    <div id="app">
		<main class="container">
            @yield('content')
        </main>
	</div>
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<!--<script src="{{ asset('plugins/jquery/jquery-3.3.1.slim.min.js') }}"></script>
	<script src="{{ asset('plugins/jquery/jquery-3.4.1.min.js') }}"></script>-->
	<!-- Bootstrap tether Core JavaScript
	<script src="{{ asset('plugins/popper/popper.min.js') }}"></script>-->
	<!-- Toastr
	<script src="{{ asset('plugins/toastr/js/toastr.min.js') }}"></script>-->
	<!--Custom JavaScript -->
	<script src="{{ asset('js/app.js') }}" defer></script>
	<!--<script src="{{ asset('js/main.js') }}"></script>-->
	@yield('scripts')
@include('shared.footer')
