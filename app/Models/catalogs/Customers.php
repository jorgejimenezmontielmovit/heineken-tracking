<?php

namespace App\Models\catalogs;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
		protected $table = "cat_customer";
		protected $primaryKey = 'id_customer';
		public $timestamps = false;
}
