@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
	<!-- ------------------------------------------------- -->
	<!-- TITULO -->
	<div class="d-inline-flex mb-3" style="width: 100%;">
		<h4>Products
		</h4>
		<h6 class="ml-auto">Admin > Catalogs > Products</h6>
	</div>
	<!-- ------------------------------------------------- -->

	<!-- ------------------------------------------------- -->
	<!-- RESULTADOS -->
	<div class="row justify-content-between pt-3">
		<div class="col col-3 d-inline-flex  generalform">
			<h3 class="card-title-2">Search:</h3> <input type="text">
		</div>
		<div class="col col-3 float-right align-end">
			<div class="d-inline-flex">
				<button class="button-ml" id="otro">
					<i class="fa fa-file-excel-o" aria-hidden="true"> Excel</i>
				</button>
				<button class="button-ml" id="otro" data-toggle="modal" data-target=".modal_product_new">
					<i class="fa fa-plus" aria-hidden="true"> New</i>
				</button>
			</div>
		</div>
	</div>
	
	<!-- Table -->
	<div class="card card-table generalform">
		<div class="card-body">

			<div class="row justify-content-between pt-1">
				<div class="col col-12">

					<div class="table_sticky">
						<table class="table">
							<thead class="">
								<tr>
									<th>ID Product</th>
									<th>Name</th>
									<th>SKU</th>
									<th>Loading</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($products as $obj)
								<tr>
									<td>{{$obj->id_product}}</td>
									<td>{{$obj->name}}</td>
									<td>{{$obj->SKU}}</td>
									<td>{{$obj->loading}}</td>
									<td><i class="fa fa-pencil" aria-hidden="true" data-toggle="modal" data-target=".modal_product_new"></i></td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>

				</div>
			</div>

		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade modal_product_new" id="modalNewProduct" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl p-4" style="max-width: 60vw !important;" role="document">
			<div class="modal-content p-3">

				<div class="row justify-content-center pt-1 mb-4 pl-4 pr-4">
					<div class="col col-lg-12">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="row justify-content-between pt-1">
							<div class="col col-lg-6">
								<div class="group-generalform">
									<input id="sku" type="text" required />
									<label for="sku">SKU</label>
								</div>
							</div>
							<div class="col col-lg-6">
								<div class="group-generalform">
									<input id="name" type="text" required />
									<label for="name">Name</label>
								</div>
							</div>
						</div>
						<div class="row justify-content-between pt-1 ">
							<div class="col col-lg-6">
								<div class="group-generalform">
									<input id="pallets" type="number" required />
									<label for="pallets">Pallets</label>
								</div>
							</div>
							<div class="col col-lg-6">
								<div class="group-generalform">
									<input id="loading" type="number" required />
									<label for="loading">Loading</label>
								</div>
							</div>
							<div class="col col-lg-6">
								<div class="group-generalform generalform">
									<p style="font-size: 13px; color: #999; margin-left:10px;">Origin Source</p>
									<select name="" id="id_origin_source" style="width:100% !important;">
										<option selected="true" value="" disabled>Select one</option>    
										@foreach ($sources as $obj)
											<option value="{{$obj->id_source}}">{{$obj->name}}</option>    
										@endforeach
									</select>
								</div>
							</div>
							<div class="col col-lg-6">
								<div class="group-generalform generalform">
									<p style="font-size: 13px; color: #999; margin-left:10px;">Shipment Type</p>
									<select name="" id="id_shipment_type" style="width:100% !important;">
										<option selected="true" value="" disabled>Select one</option>    
										@foreach ($shipmentType as $obj)
											<option value="{{$obj->id_shipment}}">{{$obj->description}}</option>    
										@endforeach
									</select>
								</div>
							</div>
							<div class="col col-lg-6">
								<div class="group-generalform generalform">
									<p style="font-size: 13px; color: #999; margin-left:10px;">Container Type</p>
									<select name="" id="id_container_type" style="width:100% !important;">
										<option selected="true" value="" disabled>Select one</option>    
										@foreach ($containerType as $obj)
											<option value="{{$obj->id_container}}">{{$obj->description}}</option>    
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row justify-content-end pt-3">
							<div class="col col-lg-3">
								<button id="submitProducts" data-dismiss="modal" class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<i class="mdi mdi-content-save"></i> Save
								</button>
							</div>
						</div>

					</div>
				</div>
				<!-- <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> -->
			</div>
		</div>
	</div>


	<!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
@section('scripts')
<script>
	let url = "{{url('catalogs/products/')}}";
	let csrfToken = "{{ csrf_token() }}";
</script>
<script src="{{ URL::asset('js/scripts/catalogs/products.js') }}"></script>
@endsection
@endsection