@extends("layouts.app")

@section("content")
<!-- ----------------------------------------------------------------------------------------------- -->
<div class="page-wrapper">
    <!-- ------------------------------------------------- -->
    <!-- TITULO -->
    <div class="d-inline-flex mb-3" style="width: 100%;">
        <h4>Stage</h4>
        <h6 class="ml-auto">Admin > Orders > Stage</h6>
    </div>
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- FILTROS -->
    <div class="card">
        <div class="card-body">
            <div class="row  pb-3">
                <div class="col-12">
                    <button class="btn button-ml-filter d-inline-flex" type="button" data-toggle="collapse"
                        data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                        style="align-items: center;">
                        <i class="fa fa-filter" aria-hidden="true" style="margin-right: 5px;"> </i> Add Filters <i
                            class="fa fa-caret-down" aria-hidden="true" style="margin-left: 5px;"> </i>
                    </button>
                </div>
                <div class="ml-3 mr-3">
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Start Date "2000-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-primary">
                        Finish Date "2099-01-01"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                    <h5 href="#" class="badge badge-pill badge-success">
                        Customers "All"
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </h5>
                </div>
            </div>


            <div id="collapseExample" class="collapse show">
            <hr>
                <form action="">
                    <div class="generalform ml-5 mr-5">

                        <div class="row justify-content-between pt-1">
                            <div class="col col-lg-12">
                                <div class="row justify-content-between pt-1">
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Start Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Final Date:</h5>
                                            <input id="" name="" type="date">
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Customer:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="col col-lg-3">
                                        <label for="">
                                            <h5>Stage:</h5>
                                            <select id="cobro" name="">
                                                <option value="0">Select One</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-end pt-3">
                            <div class="col col-lg-3">
                                <div class="btn btn-danger float-right" id="ejecutar" data-toggle="collapse"
                                    href="#collapseExample" role="button" aria-expanded="true"
                                    aria-controls="collapseExample">
                                    Apply <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- ------------------------------------------------- -->

    <!-- ------------------------------------------------- -->
    <!-- RESULTADOS -->
    <div class="row justify-content-between pt-3">
        <div class="col col-3 d-inline-flex  generalform">
            <h3 class="card-title-2">Search:</h3> <input type="text">
        </div>
        <div class="col col-3 float-right align-end">
            <div class="d-inline-flex">
                <button class="button-ml d-inline-flex" style="padding:10px 15px;" id="otro">
                    <i class="fa fa-file-excel-o" aria-hidden="true"> </i><h6 class="ml-2 mb-0"> Excel</h6>
                </button>
                <button class="button-ml d-inline-flex" style="padding:10px 15px;" id="otro">
                    <i class="fa fa-plus" aria-hidden="true"> </i><h6 class="ml-2 mb-0"> New</h6>
                </button>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">

            <div class="col col-lg-12 ">
                <div class="table_sticky">
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th>Order Number</th>
                                <th>Sold To</th>
                                <th>Ship To</th>
                                <th>Heineken PO</th>
                                <th>Requested ETD</th>
                                <th>Requested ETA</th>
                                <th>Stage</th>
                                <th>Date_Stage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1111</td>
                                <td>61557_Beverage Marketing Services Ltd.</td>
                                <td>Panamá, Panamá</td>
                                <td>1234</td>
                                <td>10/10/2019</td>
                                <td>10/10/2019</td>
                                <td>invoice booking</td>
                                <td>01/08/2019</td>
                            </tr>
                            <tr>
                                <td>1111</td>
                                <td>61557_Beverage Marketing Services Ltd.</td>
                                <td>Panamá, Panamá</td>
                                <td>1234</td>
                                <td>10/10/2019</td>
                                <td>10/10/2019</td>
                                <td>invoice booking</td>
                                <td>01/08/2019</td>
                            </tr>
                            <tr>
                                <td>1111</td>
                                <td>61557_Beverage Marketing Services Ltd.</td>
                                <td>Panamá, Panamá</td>
                                <td>1234</td>
                                <td>10/10/2019</td>
                                <td>10/10/2019</td>
                                <td>invoice booking</td>
                                <td>01/08/2019</td>
                            </tr>
                            <tr>
                                <td>1111</td>
                                <td>61557_Beverage Marketing Services Ltd.</td>
                                <td>Panamá, Panamá</td>
                                <td>1234</td>
                                <td>10/10/2019</td>
                                <td>10/10/2019</td>
                                <td>invoice booking</td>
                                <td>01/08/2019</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <!-- ------------------------------------------------- -->
</div>
<!-- ----------------------------------------------------------------------------------------------- -->
@endsection