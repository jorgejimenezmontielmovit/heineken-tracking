<?php

namespace App\Models\catalogs;

use Illuminate\Database\Eloquent\Model;

class Shipmenttype extends Model
{
	protected $table = "cat_shipment_type";
	protected $primaryKey = 'id_shipment';
	public $timestamps = false;
}
