<div class="sidebar">
            <div class="accordion mt-4" id="sidebar-menu">

            <!-- ------------------------------------------------------------------------------------------- -->
                <!-- Element Menu -->
                <div id="index" class="menu-element ">
                    <!-- ---------------------------------------------------------- -->
                    <!-- Text Menu -->
                    <i class="fa fa-cubes" aria-hidden="true"> </i>
                    <h6><a href="{{ route('dashboard') }}">Dashboard</a></h6>
                    <!-- ---------------------------------------------------------- -->
                </div>
                <!-- ------------------------------------------------------------------------------------------- -->

                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- Element Menu -->
                <div id="Orders" class="menu-element pointer" data-toggle="collapse" data-target="#collapse_Orders"
                    aria-expanded="true" aria-controls="collapse_Orders">
                    <!-- ---------------------------------------------------------- -->
                    <!-- Text Menu -->
                    <i class="fa fa-pencil" aria-hidden="true"> </i>
                    <h6>Orders</h6>
                    <!-- ---------------------------------------------------------- -->
                    <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                </div>

                <div id="collapse_Orders" class="stage collapse" aria-labelledby="Orders" data-parent="#sidebar-menu">
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="stage">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('dashboard') }}">Stage</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="new">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('dashboard') }}">New</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="tracking">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('dashboard') }}">Tracking</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                </div>
                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- Element Menu -->
                <div id="Catalogs" class="menu-element pointer" data-toggle="collapse" data-target="#collapse_Catalogs"
                    aria-expanded="true" aria-controls="collapse_Catalogs">
                    <!-- ---------------------------------------------------------- -->
                    <!-- Text Menu -->
                    <i class="fa fa-book" aria-hidden="true"> </i>
                    <h6>Catalogs</h6>
                    <!-- ---------------------------------------------------------- -->
                    <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                </div>

                <div id="collapse_Catalogs" class="collapse" aria-labelledby="Catalogs" data-parent="#sidebar-menu">
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="customers">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('customers') }}">Customers</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="products">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('products') }}">Products</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="portafolio">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('portfolio') }}">Portafolio</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <!--<div class="sub menu-element" id="locations">
                        <h6><a href="{{ route('locations') }}">Locations</a> </h6>
                    </div>-->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="shipmenttype">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('shipment') }}">Shipment Type</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="containertype">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('container') }}">Container Type</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="sources">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('sources') }}">Sources</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="leadtime">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('leadtime') }}">Lead Time</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                </div>
                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- Element Menu -->
                <div id="Administration" class="menu-element pointer" data-toggle="collapse"
                    data-target="#collapse_Administration" aria-expanded="true" aria-controls="collapseOne1">
                    <!-- ---------------------------------------------------------- -->
                    <!-- Text Menu -->
                    <i class="fa fa-user-plus" aria-hidden="true"> </i>
                    <h6>Administration</h6>
                    <!-- ---------------------------------------------------------- -->
                    <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                </div>

                <div id="collapse_Administration" class="collapse" aria-labelledby="Administration"
                    data-parent="#sidebar-menu">
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- Element Sub - Menu -->
                    <div class="sub menu-element" id="user">
                        <!-- Text Sub - Menu -->
                        <h6><a href="{{ route('users') }}">User</a> </h6>
                    </div>
                    <!-- ------------------------------------------------------------------------------------------- -->
                    <!-- ------------------------------------------------------------------------------------------- -->
                </div>
                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- ------------------------------------------------------------------------------------------- -->
                <!-- Element Menu -->
                <div id="reports" class="menu-element">
                    <!-- ---------------------------------------------------------- -->
                    <!-- Text Menu -->
                    <i class="fa fa-archive" aria-hidden="true"> </i>
                    <h6><a href="{{ route('dashboard') }}">Reports</a></h6>
                    <!-- ---------------------------------------------------------- -->
                </div>
                <!-- ------------------------------------------------------------------------------------------- -->

            </div>
        </div>