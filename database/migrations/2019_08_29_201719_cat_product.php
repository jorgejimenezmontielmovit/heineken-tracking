<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CatProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_product', function (Blueprint $table) {
            $table->smallIncrements('id_product')->index();
            $table->smallInteger('id_origin_source')->unsigned();
            $table->smallInteger('id_shipment_type')->unsigned();
            $table->smallInteger('id_container_type')->unsigned();
            $table->string('SKU',20);
            $table->string('name',60);
            $table->string('brand',30);
            $table->smallInteger('pallets');
            $table->tinyInteger('loading');
            $table->foreign('id_origin_source')->references('id_source')->on('cat_source');
            $table->foreign('id_shipment_type')->references('id_shipment')->on('cat_shipment_type');
            $table->foreign('id_container_type')->references('id_container')->on('cat_container_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_product');
    }
}
