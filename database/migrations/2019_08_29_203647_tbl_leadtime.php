<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblLeadtime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_leadtime', function (Blueprint $table) {
            $table->smallInteger('id_customer')->unsigned();
            $table->smallInteger('id_source')->unsigned();
            $table->smallInteger('lead_time');
            $table->foreign('id_customer')->references('id_customer')->on('cat_customer');
            $table->foreign('id_source')->references('id_source')->on('cat_source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_leadtime');
    }
}
