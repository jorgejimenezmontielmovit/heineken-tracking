
window.onload = function(){

	document.getElementById("submitProducts").addEventListener("click", function(){

		let data = {
			id_origin_source: document.getElementById("id_origin_source").value,
			id_shipment_type: document.getElementById("id_shipment_type").value,
			id_container_type: document.getElementById("id_container_type").value,
			SKU: document.getElementById("sku").value,
			name: document.getElementById("name").value,
			pallets: document.getElementById("pallets").value,
			loading: document.getElementById("loading").value
		};

		if(!simpleValidation(data)){
			alert("You must fill all the fields in the form");
		}else{
			request("post", url + "/create/", data, resp => {
				if(resp.success){
					alert("The product was added succesfully");
					location.reload();
				}
				else  alert("There was an error adding the product")
			});
		}

	})

}

// check if all fields have a value
function simpleValidation(obj){
	for(value in obj)
		if(obj[value] == "") return false;
	return true;
}

function request(method, url, data, response){
	$.ajax({
		headers:{
			'X-CSRF-Token': csrfToken
		},
		method: method,
		url: url,
		data: data,
		success: response,
		dataType: "json"
	});
}