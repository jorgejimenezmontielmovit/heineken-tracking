<?php

namespace App\Http\Controllers\catalogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{

		public function index()
		{
			return view("pages.catalogs.customers");
		}

		public function create()
		{
			return "";
		}

		public function read($id = null)
		{
			return $id;
		}

		public function update($id)
		{
			return $id;
		}

		public function delete($id)
		{
			return $id;
		}
}
