<?php

namespace App\Http\Controllers\catalogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\catalogs\Containertype;
use App\Models\catalogs\Products;
use App\Models\catalogs\Shipmenttype;
use App\Models\catalogs\Sources;
use stdClass;

class ProductsController extends Controller
{

		public function index()
		{
			$shipmentType = Shipmenttype::All();
			$containerType = Containertype::All();
			$sources = Sources::All();
			$products = Products::All();
			return view("pages.catalogs.products", 
			['products' => $products, 'shipmentType' => $shipmentType, 'containerType' => $containerType, 'sources' => $sources]);
		}

		// Some ways to read the input are: $request->all(), $request->input('field1')
		public function create(Request $request)
		{
			$obj = new stdClass;
			$obj->success = false;
			$model = new Products();
			$fields = ['id_origin_source', 'id_shipment_type', 'id_container_type', 'SKU', 'name', 'pallets', 'loading'];

			if($request->has($fields))
			{

				foreach($fields as $field)
				{
					$model->$field = $request->input($field);
				}

				$result = $model->save();
				if($result)
				{
					$obj->success = true;
				}

			}

			return json_encode($obj);
		}

		public function read(Request $request, $id = null)
		{
			$input = $request->input();
			$obj = new stdClass;
			$obj->success = false;
			$obj->input = $input;

			if(!empty($id))
			{
				$model = Products::find($id);
				if($model)
				{
					$obj->data = $model;
					$obj->success = true;
				}
			}
			else
			{
				$model = Products::all();
				if(count($model))
				{
					$obj->data = $model;
					$obj->success = true;
				}
			}

			return json_encode($obj);

		}

		public function update(Request $request, $id)
		{
			
			$input = $request->input();
			$model = Products::find($id);
			// .. several changes
			$model->save();
		}

		public function delete($id)
		{
			$obj = new stdClass;
			$obj->success = false;
			$model = Products::find($id);
			if($model)
			{
				$result = $model->delete();
				if($result)
				{
					$obj->success = true;
				}
			}
			return json_encode($obj);
		}
		
}
