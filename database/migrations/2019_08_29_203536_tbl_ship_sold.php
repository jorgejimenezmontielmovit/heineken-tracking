<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblShipSold extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_ship_sold', function (Blueprint $table) {
            $table->smallInteger('id_sold')->unsigned();
            $table->smallInteger('id_ship')->unsigned();
            $table->foreign('id_sold')->references('id_sold_to')->on('cat_sold_to');
            $table->foreign('id_ship')->references('id_ship_to')->on('cat_ship_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_ship_sold', function (Blueprint $table) {
            //
        });
    }
}
